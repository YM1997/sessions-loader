package com.spilnasprava.sessionsloader.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * Created by YaroslaV on 13.03.2018.
 */

@Component
@Getter
@Setter
@NoArgsConstructor
public class AuthenticationResponse {
    private HttpStatus status;
    private String massage;
    private String token;
}
