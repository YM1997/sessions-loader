package com.spilnasprava.sessionsloader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SessionsLoaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SessionsLoaderApplication.class, args);
    }

}
