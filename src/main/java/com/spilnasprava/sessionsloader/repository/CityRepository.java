package com.spilnasprava.sessionsloader.repository;

import com.spilnasprava.sessionsloader.model.city.City;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by YaroslaV on 14.03.2018.
 */
public interface CityRepository extends JpaRepository<City, Integer> {
    City findByName(String name);
}
