package com.spilnasprava.sessionsloader.repository;

import com.spilnasprava.sessionsloader.model.segment.Segment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by YaroslaV on 14.03.2018.
 */
public interface SegmentRepository extends PagingAndSortingRepository<Segment, Integer> {
    @Query(value="SELECT * FROM public.segments WHERE city_id=?1", nativeQuery = true)
    List<Segment> findByCity(int id);

    @Query(value = "SELECT * FROM public.segments WHERE (ST_Distance_Sphere(way, ST_MakePoint(?1,?2))) <= (?3)", nativeQuery = true)
    List<Segment> findByPoints(double lon, double lat, double radius);
}
