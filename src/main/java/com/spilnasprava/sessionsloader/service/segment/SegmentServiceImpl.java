package com.spilnasprava.sessionsloader.service.segment;

import com.spilnasprava.sessionsloader.model.segment.Segment;
import com.spilnasprava.sessionsloader.repository.SegmentRepository;
import com.spilnasprava.sessionsloader.service.city.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by YaroslaV on 06.03.2018.
 */
@Service
@Transactional
public class SegmentServiceImpl implements SegmentService {

    @Autowired
    private final SegmentRepository segmentRepository;

    @Autowired
    private CityService cityService;

    public SegmentServiceImpl(SegmentRepository segmentRepository) {
        this.segmentRepository = segmentRepository;
    }


    @Override
    public void saveSegments(Segment segment) {
        segmentRepository.save(segment);
    }

    @Override
    public List<Segment> findByCity(String name) {
        return segmentRepository.findByCity(cityService.findByName(name).getId());
    }

    @Override
    public Page<Segment> findAll(int page, int size) {
        return segmentRepository.findAll(new PageRequest(page,size));
    }

    @Override
    public List<Segment> findByPoints(double lon, double lat, double radius) {
        return segmentRepository.findByPoints(lon, lat, radius);
    }
}
