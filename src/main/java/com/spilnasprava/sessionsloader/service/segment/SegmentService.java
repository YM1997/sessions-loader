package com.spilnasprava.sessionsloader.service.segment;

import com.spilnasprava.sessionsloader.model.segment.Segment;
import org.springframework.data.domain.Page;

import java.util.List;


/**
 * Created by YaroslaV on 07.03.2018.
 */
public interface SegmentService {
    void saveSegments(Segment segment);
    List<Segment> findByCity(String name);
    Page<Segment> findAll(int page, int size);
    List<Segment> findByPoints(double lat, double lon, double radius);
}
