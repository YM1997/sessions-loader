package com.spilnasprava.sessionsloader.service.user;

import com.spilnasprava.sessionsloader.model.User;
import com.spilnasprava.sessionsloader.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by YaroslaV on 06.03.2018.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    static Logger log = Logger.getLogger(UserServiceImpl.class.getName());
    @Autowired
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void saveUser(User user) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String hashedPassword = encoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
        userRepository.save(user);
    }

    @Override
    public boolean findByName(String name, String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        List<User> list=userRepository.findByName(name);
        for (int i=0; i<list.size();i++){
            if(encoder.matches(password,list.get(i).getPassword())){
                return true;
            }
        }
        return false;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }


}
