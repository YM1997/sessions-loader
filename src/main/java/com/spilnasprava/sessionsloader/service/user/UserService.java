package com.spilnasprava.sessionsloader.service.user;

import com.spilnasprava.sessionsloader.model.User;

import java.util.List;


/**
 * Created by YaroslaV on 07.03.2018.
 */
public interface UserService {
    void saveUser(User user);
    boolean findByName(String name, String password);
    List<User> findAll();
}
