package com.spilnasprava.sessionsloader.service.city;

import com.spilnasprava.sessionsloader.model.city.City;

import java.util.List;


/**
 * Created by YaroslaV on 07.03.2018.
 */
public interface CityService {
    City findByName(String name);
    void saveCity(City city);
    List<City> findAll();
}
