package com.spilnasprava.sessionsloader.service.city;

import com.spilnasprava.sessionsloader.model.city.City;
import com.spilnasprava.sessionsloader.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by YaroslaV on 06.03.2018.
 */
@Service
@Transactional
public class CityServiceImpl implements CityService {

    @Autowired
    private final CityRepository cityRepository;

    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public City findByName(String name) {
        return cityRepository.findByName(name);
    }

    @Override
    public void saveCity(City city) {
        cityRepository.save(city);
    }

    @Override
    public List<City> findAll() {
        return cityRepository.findAll();
    }

}
