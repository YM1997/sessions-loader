package com.spilnasprava.sessionsloader.model.city;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by YaroslaV on 14.03.2018.
 */
@Entity
@Table(name = "cities")
@Setter
@Getter
@NoArgsConstructor
public class City {
    @Id
    private int id;
    private String name;
    private String prefix;
    private double leftLatitude;
    private double rightLatitude;
    private double leftLongitude;
    private double rightLongitude;
    private String originalName;

}
