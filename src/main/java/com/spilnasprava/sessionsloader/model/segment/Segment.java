package com.spilnasprava.sessionsloader.model.segment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.spilnasprava.sessionsloader.model.city.City;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by YaroslaV on 14.03.2018.
 */
@Entity
@Table(name = "segments")
@Setter
@Getter
@NoArgsConstructor
public class Segment {

    @Id
    private long id;
    private String fromNodeCoords;
    private String toNodeCoords;
    private String way;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

}
