package com.spilnasprava.sessionsloader.controller;

import com.spilnasprava.sessionsloader.model.User;
import com.spilnasprava.sessionsloader.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by YaroslaV on 12.03.2018.
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService service;

    @GetMapping("/find-all")
    public List<User> findAll() {
        return service.findAll();
    }
}
