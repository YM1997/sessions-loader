package com.spilnasprava.sessionsloader.controller;

import com.spilnasprava.sessionsloader.model.User;
import com.spilnasprava.sessionsloader.response.AuthenticationResponse;
import com.spilnasprava.sessionsloader.security.JwtGenerator;
import com.spilnasprava.sessionsloader.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by YaroslaV on 12.03.2018.
 */
@RestController
@RequestMapping("/token")
public class AuthenticationController {
    private JwtGenerator jwtGenerator;

    @Autowired
    private UserService userService;

    @Autowired
    AuthenticationResponse response;

    public AuthenticationController(JwtGenerator jwtGenerator) {
        this.jwtGenerator = jwtGenerator;
    }

    @PostMapping
    public AuthenticationResponse generate(@RequestBody User jwtUser) {
       if ((userService.findByName(jwtUser.getName(), jwtUser.getPassword()))==true){
           response.setStatus(HttpStatus.OK);
           response.setMassage("You are logged in");
           response.setToken(jwtGenerator.generate(jwtUser));
           return response;
       }
        else {
            response.setStatus(HttpStatus.resolve(500));
            response.setMassage("Login failed");
            response.setToken(null);
            return response;
       }
    }
}
