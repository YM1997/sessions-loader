package com.spilnasprava.sessionsloader.controller;

import com.spilnasprava.sessionsloader.model.User;
import com.spilnasprava.sessionsloader.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by YaroslaV on 07.03.2018.
 */
@RestController
@RequestMapping("/registration/")
public class RegistrationController {

    @Autowired
    private UserService userService;

    @PostMapping("user")
    public void registrationStatus(@RequestBody User user) {
        userService.saveUser(user);
    }
}
