package com.spilnasprava.sessionsloader.controller;

import com.spilnasprava.sessionsloader.model.city.City;
import com.spilnasprava.sessionsloader.service.city.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by YaroslaV on 14.03.2018.
 */
@RestController
@RequestMapping("/user/cities")
public class CityController {

    @Autowired
    private CityService cityService;

    @PostMapping
    public void addCity(@RequestBody City city){
        cityService.saveCity(city);
    }

    @GetMapping
    public List<City> findAll(){
        return cityService.findAll();
    }
}
