package com.spilnasprava.sessionsloader.controller;

import com.spilnasprava.sessionsloader.model.segment.Segment;
import com.spilnasprava.sessionsloader.service.segment.SegmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by YaroslaV on 14.03.2018.
 */
@RestController
@RequestMapping("/user/segments")
public class SegmentController {

    @Autowired
    private SegmentService segmentService;

    @PostMapping
    public void addSegment(@RequestBody Segment segment){
        segmentService.saveSegments(segment);
    }

    @GetMapping
    public Page<Segment> findAll(@RequestParam ("page") int page,@RequestParam ("size") int size){
        return segmentService.findAll(page,size);
    }

    @GetMapping("/by-city")
    public List<Segment> findByCity(@RequestParam ("city") String city){
        return segmentService.findByCity(city);
    }

    @GetMapping("/by-points")
    public List<Segment> findByPoints(@RequestParam("lat") double lat, @RequestParam("lon") double lon, @RequestParam("radius") double radius ){
        return segmentService.findByPoints(lon, lat, radius);
    }
}
